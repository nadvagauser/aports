# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=swaync
_projname=SwayNotificationCenter
pkgver=0.7.1
pkgrel=0
pkgdesc="Notification daemon with a GTK GUI for notifications and the control center"
url="https://github.com/ErikReider/SwayNotificationCenter"
arch="all !riscv64 !s390x"  # blocked by libhandy1
license="GPL-3.0"
depends="dbus"
makedepends="
	gobject-introspection-dev
	gtk+3.0-dev
	gtk-layer-shell-dev
	json-glib-dev
	libhandy1-dev
	meson
	scdoc
	vala
	"
subpackages="
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/ErikReider/SwayNotificationCenter/archive/v$pkgver/$pkgname-$pkgver.tar.gz
	fix-fish-completions-dir.patch
	"
builddir="$srcdir/$_projname-$pkgver"
options="!check"  # no tests defined

build() {
	abuild-meson \
		-Dsystemd-service=false \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
02f655f2faa7055332b44a8d7113c7019f280a0dcd1aedcba6e2dbf15a1661fd6b71b8307e3874e2909437b678cc8c7a3fc5fc3f18c8b43210078fc46ccd98c8  swaync-0.7.1.tar.gz
d6adceae7d8cba6ca6a3c131e61ac7ef31edacdd847063555fb5827494d4145d260975c8955492b7739cac9857457557e1f30870713468ced60f9b56c5534b55  fix-fish-completions-dir.patch
"
