# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=onevpl
pkgver=2022.2.3
pkgrel=0
pkgdesc="oneAPI Video Processing Library"
url="https://github.com/oneapi-src/oneVPL"
arch="x86_64" # only x86_64 supported
license="MIT"
makedepends="cmake samurai"
subpackages="$pkgname-doc $pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/oneapi-src/oneVPL/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/oneVPL-$pkgver"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DONEAPI_INSTALL_ENVDIR=/usr/share/oneVPL/env \
		-DONEAPI_INSTALL_LICENSEDIR=/usr/share/doc/oneVPL \
		-DONEAPI_INSTALL_MODFILEDIR=/usr/share/oneVPL/modulefiles \
		-DBUILD_PREVIEW=OFF \
		-DBUILD_EXAMPLES=OFF \
		-DBUILD_TOOLS=OFF \
		-DINSTALL_EXAMPLE_CODE=OFF \
		-DBUILD_TESTS="$(want_check && echo ON || echo OFF)"
	cmake --build build
}

check() {
	ctest -j $JOBS --output-on-failure --test-dir build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

dev() {
	default_dev

	amove usr/share
}

sha512sums="
010e465be2e32865e9e8f30a6454b1b1814ad29db023c70c6a74792bcd953b3ed756831b4a3a6c0907dd57cb9ef40a7877236303879172226bfd4e3b4ccccf95  onevpl-2022.2.3.tar.gz
"
